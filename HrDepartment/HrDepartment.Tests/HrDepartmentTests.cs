using NUnit.Framework;
using Moq;
using HrDepartment.Application.Services;
using HrDepartment.Application.Interfaces;
using HrDepartment.Domain.Entities;
using System.Threading.Tasks;

namespace HrDepartment.Tests
{
	public class HrDepartmentTests
	{

		[SetUp]
		public void Setup()
		{

		}

		[Test]
		public void CalculateJobSeekerRatingAsync_JobSeekerStandart_WorkAsExpected()
		{
			var js = new JobSeeker
			{
				Id = 22,
				FirstName = "Andrey",
				LastName = "An",
				MiddleName = "Anov",
				BadHabits = Domain.Enums.BadHabits.Smoking,
				Education = Domain.Enums.EducationLevel.School,
				BirthDate = new System.DateTime(2000, 4, 29),
				Experience = 2,
				Status = Domain.Enums.JobSeekerStatus.New
			};

			Mock<ISanctionService> mocksanc = new Mock<ISanctionService>();
			Mock<JobSeekerRateService> mockrate = new Mock<JobSeekerRateService>(mocksanc.Object);

			var rating = mockrate.Object.CalculateJobSeekerRatingAsync(js).Result;

			Assert.AreEqual(rating, 30);
		}

		[Test]
		public void CalculateJobSeekerRatingAsync_JobSeekerNoEducationDrugs_WorkAsExpected()
		{
			var js = new JobSeeker
			{
				Id = 22,
				FirstName = "Andrey",
				LastName = "An",
				MiddleName = "Anov",
				BadHabits = Domain.Enums.BadHabits.Drugs,
				Education = Domain.Enums.EducationLevel.None,
				BirthDate = new System.DateTime(2000, 4, 29),
				Experience = 0,
				Status = Domain.Enums.JobSeekerStatus.New
			};

			Mock<ISanctionService> mocksanc = new Mock<ISanctionService>();
			Mock<JobSeekerRateService> mockrate = new Mock<JobSeekerRateService>(mocksanc.Object);

			var rating = mockrate.Object.CalculateJobSeekerRatingAsync(js).Result;

			Assert.AreEqual(rating, 0);
		}

		[Test]
		public void CalculateJobSeekerRatingAsync_JobSeekerExperiencedSuperman_WorkAsExpected()
		{
			var js = new JobSeeker
			{
				Id = 22,
				FirstName = "Andrey",
				LastName = "An",
				MiddleName = "Anov",
				BadHabits = Domain.Enums.BadHabits.None,
				Education = Domain.Enums.EducationLevel.University,
				BirthDate = new System.DateTime(1977, 4, 29),
				Experience = 15,
				Status = Domain.Enums.JobSeekerStatus.New
			};

			Mock<ISanctionService> mocksanc = new Mock<ISanctionService>();
			Mock<JobSeekerRateService> mockrate = new Mock<JobSeekerRateService>(mocksanc.Object);

			var rating = mockrate.Object.CalculateJobSeekerRatingAsync(js).Result;

			Assert.AreEqual(rating, 95);
		}

		[Test]
		public void CalculateJobSeekerRatingAsync_JobSeekerExperienced4years_WorkAsExpected()
		{
			var js = new JobSeeker
			{
				Id = 22,
				FirstName = "Andrey",
				LastName = "An",
				MiddleName = "Anov",
				BadHabits = Domain.Enums.BadHabits.None,
				Education = Domain.Enums.EducationLevel.University,
				BirthDate = new System.DateTime(1977, 4, 29),
				Experience = 4,
				Status = Domain.Enums.JobSeekerStatus.New
			};

			Mock<ISanctionService> mocksanc = new Mock<ISanctionService>();
			Mock<JobSeekerRateService> mockrate = new Mock<JobSeekerRateService>(mocksanc.Object);

			var rating = mockrate.Object.CalculateJobSeekerRatingAsync(js).Result;

			Assert.AreEqual(rating, 70);
		}

		[Test]
		public void CalculateJobSeekerRatingAsync_JobSeekerExperienced_WorkAsExpected()
		{
			var js = new JobSeeker
			{
				Id = 22,
				FirstName = "Andrey",
				LastName = "An",
				MiddleName = "Anov",
				BadHabits = Domain.Enums.BadHabits.None,
				Education = Domain.Enums.EducationLevel.College,
				BirthDate = new System.DateTime(1977, 4, 29),
				Experience = 6,
				Status = Domain.Enums.JobSeekerStatus.New
			};

			Mock<ISanctionService> mocksanc = new Mock<ISanctionService>();
			Mock<JobSeekerRateService> mockrate = new Mock<JobSeekerRateService>(mocksanc.Object);

			var rating = mockrate.Object.CalculateJobSeekerRatingAsync(js).Result;

			Assert.AreEqual(rating, 60);
		}

		[Test]
		public void CalculateJobSeekerRatingAsync_JobSeekerUnderSanction_WorkAsExpected()
		{
			var js = new JobSeeker
			{
				Id = 22,
				FirstName = "Andrey",
				LastName = "An",
				MiddleName = "Anov",
				BadHabits = Domain.Enums.BadHabits.Smoking,
				Education = Domain.Enums.EducationLevel.School,
				BirthDate = new System.DateTime(2000, 4, 29),
				Experience = 0,
				Status = Domain.Enums.JobSeekerStatus.New
			};

			Mock<ISanctionService> mocksanc = new Mock<ISanctionService>();
			mocksanc.SetupAllProperties();
			mocksanc.Setup(a => a.IsInSanctionsListAsync("An", "Andrey", "Anov", new System.DateTime(2000, 4, 29))).Returns(Task.FromResult(true));

			Mock<JobSeekerRateService> mockrate = new Mock<JobSeekerRateService>(mocksanc.Object);

			var rating = mockrate.Object.CalculateJobSeekerRatingAsync(js).Result;

			Assert.AreEqual(rating, 0);
		}

		[Test]
		public void CalculateJobSeekerRatingAsync_JobSeekerStandart_ThrowsException()
		{
			var js = new JobSeeker
			{
				Id = 22,
				FirstName = "Andrey",
				LastName = "An",
				MiddleName = "Anov",
				BadHabits = Domain.Enums.BadHabits.Smoking,
				Education = (Domain.Enums.EducationLevel)5,
				BirthDate = new System.DateTime(2000, 4, 29),
				Experience = 0,
				Status = Domain.Enums.JobSeekerStatus.New
			};

			Mock<ISanctionService> mocksanc = new Mock<ISanctionService>();
			Mock<JobSeekerRateService> mockrate = new Mock<JobSeekerRateService>(mocksanc.Object);

			Assert.ThrowsAsync<System.ArgumentOutOfRangeException>(() => mockrate.Object.CalculateJobSeekerRatingAsync(js));

		}
	}
}